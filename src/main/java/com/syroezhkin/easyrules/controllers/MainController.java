package com.syroezhkin.easyrules.controllers;

import com.syroezhkin.easyrules.models.CustomContract;
import com.syroezhkin.easyrules.rules.SimpleRulesEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by syroezhkin on 29/05/2017.
 */
@Controller
public class MainController {

    @Autowired
    private SimpleRulesEngine myRulesEngine;

    @RequestMapping(value = "test")
    @ResponseBody
    public String test() {

        CustomContract contract;

        contract = new CustomContract();
        contract.setId("123");
        contract.setName("contract123");
        myRulesEngine.process(contract);
        System.out.println(contract);

        contract = new CustomContract();
        contract.setId("234");
        contract.setName("contract234");
        myRulesEngine.process(contract);
        System.out.println(contract);

        contract = new CustomContract();
        contract.setId("567");
        myRulesEngine.process(contract);
        System.out.println(contract);

        contract = new CustomContract();
        contract.setId("789");
        contract.setName("wwwww789");
        myRulesEngine.process(contract);
        System.out.println(contract);


        contract = new CustomContract();
        contract.setId("001");
        contract.setName("wwwww789");
        myRulesEngine.process(contract);
        System.out.println(contract);


        contract = new CustomContract();
        contract.setId("001");
        contract.setName("");
        myRulesEngine.process(contract);
        System.out.println(contract);

        return "test";
    }
}

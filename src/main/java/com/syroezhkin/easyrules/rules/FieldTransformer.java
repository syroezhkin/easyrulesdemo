package com.syroezhkin.easyrules.rules;

import java.util.Optional;

/**
 * Action for modifying fields for object.
 *
 * Created by syroezhkin on 31/05/2017.
 */
public class FieldTransformer extends AbstractFieldBean implements Action {

    private Object value;

    /**
     * Field value what will be set for object by this Action.
     * @param value {@link Object}
     */
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public void process(Object object) {
        Optional.ofNullable(object).ifPresent(_o -> this.setFieldValue(_o, value));
    }
}

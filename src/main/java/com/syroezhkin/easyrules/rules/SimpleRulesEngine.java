package com.syroezhkin.easyrules.rules;

import com.syroezhkin.easyrules.models.CustomContract;

import java.util.List;

/**
 * Created by syroezhkin on 29/05/2017.
 */
public class SimpleRulesEngine {

    private List<Rule> ruleList;

    public void setRuleList(List<Rule> ruleList) {
        this.ruleList = ruleList;
    }

    public void process(CustomContract contract) {
        ruleList.stream().filter(_rule -> _rule.when(contract)).findFirst().ifPresent(_rule -> _rule.then(contract));
    }

}

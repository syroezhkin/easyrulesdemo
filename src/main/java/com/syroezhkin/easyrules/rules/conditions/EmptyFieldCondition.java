package com.syroezhkin.easyrules.rules.conditions;

import com.syroezhkin.easyrules.rules.AbstractFieldBean;
import org.springframework.util.StringUtils;

/**
 * Created by syroezhkin on 30/05/2017.
 */
public class EmptyFieldCondition extends AbstractFieldBean implements Condition{

    @Override
    public boolean check(Object object) {
        return StringUtils.isEmpty(this.getValueByFieldName(object));
    }
}

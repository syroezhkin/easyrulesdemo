package com.syroezhkin.easyrules.rules.conditions;

import java.util.List;

/**
 * Condition which provide AND boolean operator for list of conditions in properties;
 *
 * Created by syroezhkin on 30/05/2017.
 */
public class AndCondition implements Condition {

    private List<Condition> conditions;

    /**
     * Set list of {@link Condition} for process boolean operation.
     *
     * @param conditions {@link List} of {@link Condition}
     */
    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    @Override
    public boolean check(Object arg) {
        return conditions.stream().allMatch(_c -> _c.check(arg));
    }
}

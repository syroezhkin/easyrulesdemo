package com.syroezhkin.easyrules.rules.conditions;

/**
 * Condition which provide NOT boolean operator for the condition in property.
 *
 * Created by syroezhkin on 30/05/2017.
 */
public class NotCondition implements Condition {

    private Condition condition;

    /**
     * Set {@link Condition} for process boolean operation.
     *
     * @param condition {@link Condition} for boolean operation
     */
    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    @Override
    public boolean check(Object object) {
        return !condition.check(object);
    }

}

package com.syroezhkin.easyrules.rules.conditions;

import com.syroezhkin.easyrules.rules.AbstractFieldBean;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Condition for checking {@link String} field for RegEx pattern.
 *
 * Created by syroezhkin on 30/05/2017.
 */
public class RegexCondition extends AbstractFieldBean implements Condition {

    private String pattern;

    private Integer flags;

    private Pattern compiledPattern;

    /**
     * Set RegEx pattern for Condition.
     * For more details see {@link Pattern}.
     *
     * @param pattern {@link String} RegEx pattern
     */
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    /**
     * Set pattern flags.
     * For more detail see {@link Matcher}.
     *
     * @param flags {@link Integer} pattern flags
     */
    public void setFlags(Integer flags) {
        this.flags = flags;
    }

    private Pattern getCompiledPattern() {
        if (this.compiledPattern == null) {
            if (flags == null) {
                this.compiledPattern = Pattern.compile(pattern);
            } else {
                this.compiledPattern = Pattern.compile(pattern, flags);
            }
        }
        return this.compiledPattern;
    }

    @Override
    public boolean check(Object object) {
        return Optional.ofNullable(object)
                .map(_o -> (String) this.getValueByFieldName(_o))
                .map(_s -> getCompiledPattern().matcher(_s))
                .map(Matcher::matches)
                .orElse(false);
    }
}

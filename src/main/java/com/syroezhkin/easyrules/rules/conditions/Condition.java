package com.syroezhkin.easyrules.rules.conditions;

/**
 * Condition for checking in rules.
 *
 * Created by syroezhkin on 29/05/2017.
 */
public interface Condition {

    /**
     * Check <code>object</code> for executing condition.
     *
     * @param object checked object
     * @return <code>true</code> if object executes this condition, <code>false</code> if not
     */
    boolean check(Object object);
}

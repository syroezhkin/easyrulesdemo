package com.syroezhkin.easyrules.rules;

import com.syroezhkin.easyrules.rules.conditions.Condition;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.util.Optional;

/**
 * Abstract class which provides some methods for getting access to the fields of objects.
 * Can be used for implementations of {@link Condition} and {@link Action} interfaces which needs to read
 * fields of Objects or modify them.
 *
 * Created by syroezhkin on 31.05.2017.
 */
public abstract class AbstractFieldBean {

    private String fieldName;

    /**
     * Set field name which can be used in {@link Condition} and {@link Action} for reading and writing into the objects.
     *
     * @param fieldName {@link String} field name
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * Get field value as {@link Object} for the <code>object</code>.
     *
     * @param object
     * @return
     */
    protected Object getValueByFieldName(Object object) {
        return Optional.ofNullable(object).map(_o -> {
            try {
                return FieldUtils.readDeclaredField(_o, fieldName, true);
            } catch (IllegalAccessException ignored) {
                return null;
            }
        }).orElse(null);
    }

    /**
     * Set field value for object.
     *
     * @param object modified object
     * @param value new value of the field
     */
    protected void setFieldValue(Object object, Object value) {
        Optional.ofNullable(object).map(_o -> FieldUtils.getDeclaredField(_o.getClass(), this.fieldName, true))
                .ifPresent(_f -> {
                    try {
                        _f.set(object, value);
                    } catch (IllegalAccessException ignored) { }
                });
    }
}

package com.syroezhkin.easyrules.rules;

import com.syroezhkin.easyrules.rules.conditions.Condition;

import java.util.List;

/**
 * Created by syroezhkin on 29/05/2017.
 */

public class BasicRule implements Rule {

    private List<Action> actions;
    private Condition condition;

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    @Override
    public boolean when(Object arg) {
        return this.condition.check(arg);
    }

    @Override
    public void then(Object object) {
        this.actions.forEach(_a -> _a.process(object));
    }
}

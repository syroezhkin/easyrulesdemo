package com.syroezhkin.easyrules.rules;

/**
 * Rule interface provides rule behaviour.
 *
 * Created by syroezhkin on 29/05/2017.
 */
public interface Rule {

    /**
     * Checking is this rule is executed by <code>object</code>.
     *
     * @param object checked object
     * @return <code>true</code> if rule is executed, <code>false</code> if not
     */
    boolean when(Object object);

    /**
     * Behaviour of the rule when it is executed.
     *
     * @param object checked object
     */
    void then(Object object);

}

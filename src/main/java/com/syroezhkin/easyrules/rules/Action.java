package com.syroezhkin.easyrules.rules;

/**
 * Processing some action for object if the object executes conditions of the Rule.
 *
 * Created by syroezhkin on 31/05/2017.
 */
public interface Action {

    /**
     * Do some action for the object.
     *
     * @param object Processing object
     */
    void process(Object object);
}

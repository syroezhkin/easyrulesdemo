package com.syroezhkin.easyrules.rules;

import com.syroezhkin.easyrules.models.CustomContract;
import junitparams.JUnitParamsRunner;

import junitparams.Parameters;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;

/**
 * Created by syroezhkin on 30/05/2017.
 */
@RunWith(JUnitParamsRunner.class)
@ContextConfiguration(locations = {"/application-context.xml"})
public class MyRulesEngineTest {

    @ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @org.junit.Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Autowired
    private SimpleRulesEngine myRulesEngine;

    @Test
    @Parameters
    public void process(String id, String name, Integer type) {
        CustomContract contract = new CustomContract();
        contract.setId(id);
        contract.setName(name);
        myRulesEngine.process(contract);
        Assert.assertEquals(type, contract.getType());
    }

    public Object[] parametersForProcess() {
        return new Object[] {
                new Object[] {"123", "contract123", 2},
                new Object[] {"234", "contract234", 1},
                new Object[] {"234", "CONTRACT234", 1},
                new Object[] {"234", "non-contract234", null},
                new Object[] {"567", null, null},
                new Object[] {"789", "www789", null},
                new Object[] {"001", "www789", 3},
                new Object[] {"001", "", null},
        };
    }

    @Test
    @Parameters
    public void processMultiActions(String id, String name, Integer type, String nameAfter) {
        CustomContract contract = new CustomContract();
        contract.setId(id);
        contract.setName(name);
        myRulesEngine.process(contract);
        Assert.assertEquals(type, contract.getType());
        Assert.assertEquals(nameAfter, contract.getName());
    }

    public Object[] parametersForProcessMultiActions() {
        return new Object[] {
                new Object[] {"000", "", 0, "Contract000"}
        };
    }
}